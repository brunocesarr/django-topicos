from rest_framework import serializers
from .models import *

class ContaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Conta
        fields = [ 'id' , 'descricao', 'data_venc', 'valor', 'tipo', 'pago']

class RepublicaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Republica
        fields = [ 'id' , 'nome', 'genero', 'since_year', 'cidade']