# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django import forms

from painel.models import MyUser

# Create your models here.
class Republica(models.Model):
    ESTADOS = (
        ('AC', 'Acre'),
        ('AL', 'Alagoas'),
        ('AP', 'Amapá'),
        ('AM', 'Amazonas', ),
        ('BA', 'Bahia', ),
        ('CE', 'Ceará', ),
        ('DF', 'Distrito Federal'),
        ('ES', 'Espírito Santo'),
        ('GO', 'Goiás'),
        ('MA', 'Maranhão'),
        ('MT', 'Mato Grosso', ),
        ('MS', 'Mato Grosso do Sul'),
        ('MG', 'Minas Gerais',),
        ('PA', 'Pará'),
        ('PB', 'Paraíba'),
        ('PR', 'Paraná'),
        ('PE', 'Pernambuco'),
        ('PI', 'Piauí'),
        ('RJ', 'Rio de Janeiro'),
        ('RN', 'Rio Grande do Norte'),
        ('RS', 'Rio Grande do Sul'),
        ('RO', 'Rondônia'),
        ('RR', 'Roraima'),
        ('SC', 'Santa Catarina'),
        ('SP', 'São Paulo'),
        ('SE', 'Sergipe'),
        ('TO', 'Tocantins')
    )
    LOGRADOURO = (
        ('Ave', 'Avenida'),
        ('Rua', 'Rua'),
        ('Tra', 'Travessa')
    )
    GENERO = (
        ('Masc', 'Masculina'),
        ('Femi', 'Feminina'),
        ('Mist', 'Mista')
    )

    nome = models.CharField('Nome da Republica', max_length=50, null=False, blank=False)
    endereco = models.CharField('Endereço', max_length=50, null=False, blank=False)
    cidade = models.CharField('Cidade', max_length=30, null=False, blank=False)
    estado = models.CharField('Estado', max_length=2, choices=ESTADOS, null=False, blank=False)
    since_year = models.DateField('Ano', auto_now=False, auto_now_add=False, null=False, blank=False)
    genero = models.CharField('Gênero', max_length=4, choices=GENERO, null=False, blank=False)

'''
class Usuario(models.Model):
    nome = models.CharField('Nome', max_length=30, null=False, blank=False)
    sobrenome = models.CharField('Sobrenome', max_length=30, null=True, blank=True)
    username = models.CharField('Username', max_length=30, null=False, blank=False)
    email = models.EmailField('Email', null=False, blank=False)
    senha = models.CharField('Senha', max_length=50)
    id_republica = models.ForeignKey(Republica, on_delete=models.CASCADE, null=True)
'''

class Conta(models.Model):
    generos_conta = (
        ('Des', 'Despesa'),
        ('Rec', 'Receita')
    )
    
    descricao = models.CharField('Descrição', max_length=50)
    data_venc = models.DateField('Data Vencimento')
    tipo = models.CharField('Tipo da Conta', max_length=3, choices=generos_conta)
    valor = models.FloatField('Valor da Conta')
    pago = models.BooleanField('Pago')
    id_contato = models.ForeignKey(MyUser, on_delete=models.CASCADE, null=True)