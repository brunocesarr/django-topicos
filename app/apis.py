from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import generics, permissions, pagination

from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404

#   Models
from .models import *
from painel.models import MyUser

#   Serializers
from painel.serializers import *
from .serializers import *

class UsersAPI(APIView):
    def get(self, request):
        users = MyUser.objects.all()

        serializer = MyUserSerializer(users, many=True)

        return Response({
            'dados': serializer.data,
        })

    def delete(self, request, pk):
        # Get object with this pk
        user = get_object_or_404(MyUser.objects.all(), pk=pk)
        user.delete()
        return Response({"message": "User with id `{}` has been deleted.".format(pk)},status=204)

class ContasAPI(APIView):
    def get(self, request):
        contas = Conta.objects.all()

        serializer = ContaSerializer(contas, many=True)

        return Response({
            'dados': serializer.data,
        })

class RepublicaAPI(APIView):
    def get(self, request):
        republicas = Republica.objects.all()

        serializer = RepublicaSerializer(republicas, many=True)

        return Response({
            'dados': serializer.data,
        })
    def post(request):

        return Response({
            'message':'Gravado com sucesso',
        })