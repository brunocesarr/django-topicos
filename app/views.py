# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.contrib import messages

from .forms import *
from painel.forms import UserCreationForm

from django.views.decorators.csrf import csrf_protect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

# Create your views here.

def index(request):
    context = {
        'titulo' : 'Principal',

        'notificacao' : {
            'n' : 5,
            'pendente' : 3,
            'atrasada' : 2
        },
        
        'usuario' : {
            'nome' : 'Usuario',
            'created_at' : '08/10/2019',
        }
    }
    return render(request, 'painel/dashboard.html', context)

'''
@csrf_protect
def logar(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        usuario = authenticate(request, username=username, password=password)
        if usuario is not None:
            login(request, usuario)
            redirect('dashboard')
        else:
            messages.error(request, 'As credenciais do usuário estão incorretas')
            return redirect('register')
    return render(request, 'painel/dashboard.html')

def logout_view(request):
    logout(request)
    # Redirect to a success page.
    return redirect("/login")

def register(request):
    if request.method == "POST":
        repForm = RepublicaForm(request.POST)
        print(request.POST)
        if repForm.is_valid():
            repForm.save()
            messages.sucess('Cadastro efetuado com sucesso!')
            redirect('register')
    else:
        repForm = RepublicaForm()
    #return redirect('dashboard')
    return render(request, 'register_republica.html', {'form': repForm})
'''

@login_required
def dashboard(request):
    context = {
        'titulo' : 'Dashboard',

        'notificacao' : {
            'n' : 5,
            'pendente' : 3,
            'atrasada' : 2
        },

        'usuario' : {
            'created_at' : '08/10/2019',
        }
    }
    return render(request, 'painel/dashboard.html', context)

@login_required
def agenda(request):
    context = {
        'titulo' : 'Agenda',
    }
    return render(request, 'painel/agenda.html', context)

@login_required
def forum(request):
    context = {
        'titulo' : 'Forum',
    }
    return render(request, "painel/forum.html", context)

@login_required
def perfil(request):
    form = UserCreationForm(request.POST or None)

    if request.method == "GET":
        usuario = MyUser.objects.get(pk=request.user.id)
    if form.is_valid():
            form.save()
            messages.sucess('Cadastro efetuado com sucesso!')
            redirect('perfil')
    context = {
        'titulo' : 'Perfil',
        'form' : form
    }
    return render(request, "painel/perfil.html", context)

@login_required
def avisos(request):
    context = {
        'titulo' : 'Avisos',
    }
    return render(request, "painel/avisos.html", context)

@login_required
def lancamento(request):
    form = ContasCreateForm(request.POST or None)

    if form.is_valid():
        form.save()
        messages.success(request, 'Cadastro efetuado com sucesso!')
        redirect('lancamento')
    context = {
        'titulo' : 'Lancamento',
        'form' : form
    }
    return render(request, "painel/financeiro/lancamento.html", context)

@login_required
def relatorio(request):
    context = {
        'titulo' : 'Relatorio',
    }
    return render(request, "painel/financeiro/relatorio.html", context)

@login_required
def contatos(request):
    context = {
        'titulo' : 'Contatos',
    }
    return render(request, "painel/financeiro/contatos.html", context)

@login_required
def orcamento(request):
    context = {
        'titulo' : 'Orcamento',
    }
    return render(request, "painel/metas/orcamento.html", context)

@login_required
def economia(request):
    context = {
        'titulo' : 'Economia',
    }
    return render(request, "painel/metas/economia.html", context)

@login_required
def republica(request):
    context = {
        'titulo' : 'Republica',
    }
    return render(request, "painel/opcao/republica.html", context)

@login_required
def integrantes(request):
    context = {
        'titulo' : 'Integrantes',
    }
    return render(request, "painel/opcao/integrantes.html", context)

@login_required
def addIntegrante(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()

    redirect('integrantes')

@login_required
def updateIntegrante(request, pk):
    usuario = MyUser.objects.get(pk=pk)

    form = UserCreationForm(request.POST or None, instance = usuario)

    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            messages.sucess('Cadastro efetuado com sucesso!')
            
    redirect('integrantes')

@login_required
def updateIntegrante(request, pk):
    usuario = MyUser.objects.get(pk=pk)
    usuario.delete()
            
    redirect('integrantes')