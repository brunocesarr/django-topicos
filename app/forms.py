# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
import datetime

from .models import *

class RepublicaForm(forms.ModelForm):
    ESTADOS = [
        ('AC', 'Acre'),
        ('AL', 'Alagoas'),
        ('AP', 'Amapá'),
        ('AM', 'Amazonas', ),
        ('BA', 'Bahia', ),
        ('CE', 'Ceará', ),
        ('DF', 'Distrito Federal'),
        ('ES', 'Espírito Santo'),
        ('GO', 'Goiás'),
        ('MA', 'Maranhão'),
        ('MT', 'Mato Grosso', ),
        ('MS', 'Mato Grosso do Sul'),
        ('MG', 'Minas Gerais',),
        ('PA', 'Pará'),
        ('PB', 'Paraíba'),
        ('PR', 'Paraná'),
        ('PE', 'Pernambuco'),
        ('PI', 'Piauí'),
        ('RJ', 'Rio de Janeiro'),
        ('RN', 'Rio Grande do Norte'),
        ('RS', 'Rio Grande do Sul'),
        ('RO', 'Rondônia'),
        ('RR', 'Roraima'),
        ('SC', 'Santa Catarina'),
        ('SP', 'São Paulo'),
        ('SE', 'Sergipe'),
        ('TO', 'Tocantins')
    ]
    LOGRADOURO = [
        ('Ave', 'Avenida'),
        ('Rua', 'Rua'),
        ('Tra', 'Travessa')
    ]
    GENERO = (
        ('Masc', 'Masculina'),
        ('Femi', 'Feminina'),
        ('Mist', 'Mista')
    )

    estado = forms.ChoiceField(widget=forms.Select(attrs={'class':'form-control'}),choices=ESTADOS)
    genero = forms.ChoiceField(widget=forms.Select(attrs={'class':'form-control'}),choices=GENERO)
    logradouro = forms.ChoiceField(widget=forms.Select(attrs={'class':'form-control'}),choices=LOGRADOURO)
    class Meta:
        model= Republica
        fields= '__all__'

class ContasCreateForm(forms.ModelForm):
    generos_conta = (
        ('Des', 'Despesa'),
        ('Rec', 'Receita')
    )

    descricao = forms.CharField(label='Descricao', widget=forms.TextInput(attrs={'class':'form-control'}))
    #data_venc = forms.DateField(label='Data de Vencimento',initial=datetime.date.today, input_formats=['%d/%m/%Y'],
    #    widget=forms.SelectDateWidget(attrs={
    #        'class': 'form-control'
    #    }))
    tipo = forms.ChoiceField(label='Tipo da Conta', widget=forms.Select(attrs={'class':'form-control'}), choices= generos_conta)
    valor = forms.FloatField(label='Valor', widget=forms.TextInput(attrs={'class':'form-control'}))
    pago = forms.BooleanField(widget=forms.CheckboxInput(attrs={'class':'form-check-input'}))
    
    class Meta:
        model= Conta
        fields= [
            'descricao', 'data_venc', 'tipo', 'valor', 'pago'
        ]
        widgets = {
            'data_venc': forms.TextInput(attrs={'class':'form-control','type':'date'}),
        }