from django.conf.urls import url, include

from .views import *

#   API
from .apis import *

urlpatterns = [
    url(r'^painel/', include([
        url(r'^$', dashboard, name='dashboard'),
        url(r'^agenda/', agenda, name='agenda'),
        url(r'^forum/', forum, name='forum'),
        url(r'^perfil/', perfil, name='perfil'),
        url(r'^avisos/', avisos, name='avisos'),
        url(r'^financeiro/lancamento/', lancamento, name='lancamentos'),
        url(r'^financeiro/relatorio/', relatorio, name='relatorios'),
        url(r'^financeiro/contatos/', contatos, name='contatos'),
        url(r'^metas/orcamento/', orcamento, name='orcamento'),
        url(r'^metas/economia/', economia, name='economia'),

        url(r'^opcao/integrantes/', integrantes, name='integrantes'),
        url(r'^opcao/integrantes/<pk>', integrantes, name='showIntegrantes'),
        url(r'^opcao/integrantes/add', integrantes, name='addIntegrantes'),
        url(r'^opcao/integrantes/update/<pk>', integrantes, name='updateIntegrantes'),
        url(r'^opcao/integrantes/delete/<pk>', integrantes, name='deleteIntegrantes'),
            
        url(r'^opcao/republica', republica, name='republica'),
    ])),

    #   API
    url(r'^api/', include([
        url(r'^users/', UsersAPI.as_view(), name='users_api'),
        url(r'^republica/', RepublicaAPI.as_view(), name='republica_api'),
        url(r'^contas/', ContasAPI.as_view(), name='contas_api')
    ]))
] 
