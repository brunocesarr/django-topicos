
"""
core URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
    url(r'^admin/', admin.site.urls),
"""
from django.conf.urls import url, include
from django.contrib import admin

from django.conf import settings
from django.conf.urls.static import static

from painel.views import register, login_view, logout_view
from django.views.generic.base import RedirectView


urlpatterns = [
    url('admin/', admin.site.urls),
    url('', include('app.urls')),
    #url(r'^.*$', RedirectView.as_view(pattern_name='login', permanent=False)),

    url(r'^register/$', register),
    url(r'^login/$', login_view, name='login'),
    url(r'^logout/$', logout_view, name='logout')
    #url('account/', include('django.contrib.auth.urls')),
] 

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
