# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth import get_user_model
from django.db.models import Q

from django.contrib import messages
from django import forms

User = get_user_model()

class UserCreationForm(forms.ModelForm):
	username = forms.CharField(label='Username', widget=forms.TextInput(attrs={'class':'form-control'}))
	email = forms.CharField(label='Email', widget=forms.TextInput(attrs={'class':'form-control'}))
	nome = forms.CharField(label='Nome', widget=forms.TextInput(attrs={'class':'form-control'}))
	sobrenome = forms.CharField(label='Sobrenome', widget=forms.TextInput(attrs={'class':'form-control'}))
	password1 = forms.CharField(label='Senha', widget=forms.PasswordInput(attrs={'class':'form-control'}))
	password2 = forms.CharField(label='Confirmar Senha', widget=forms.PasswordInput(attrs={'class':'form-control'}))

	class Meta:
		model = User
		fields = ['username', 'email', 'nome', 'sobrenome']

	def clean_password(self):
		password1 = self.cleaned_data.get('password1')
		password2 = self.cleaned_data.get('password2')
		if password1 and password2 and password1 != password2:
			raise forms.ValidationError("Passwords do not match")
		return password2

	def save(self, commit=True):
		user = super(UserCreationForm, self).save(commit=False)
		user.set_password(self.cleaned_data['password1'])

		if commit:
			user.save()
		return user


class UserLoginForm(forms.Form):
	query = forms.CharField(label='Username / Email', widget=forms.TextInput(attrs={'class':'form-control'}))
	password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'class':'form-control'}))

	def clean(self, *args, **kwargs):
		query = self.cleaned_data.get('query')
		password = self.cleaned_data.get('password')
		user_qs_final = User.objects.filter(
				Q(username__iexact=query) |
				Q(email__iexact=query)
			).distinct()
		if not user_qs_final.exists() and user_qs_final.count != 1:
			raise forms.ValidationError("Credenciais incorretas - Usuário não existe!")
		user_obj = user_qs_final.first()
		if not user_obj.check_password(password):
			raise forms.ValidationError("Credenciais incorretas!")
		self.cleaned_data["user_obj"] = user_obj
		return super(UserLoginForm, self).clean(*args, **kwargs)
		
























