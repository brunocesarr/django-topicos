# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.contrib.auth import login, get_user_model, logout

from django.http import HttpResponseRedirect
# Create your views here.
from .forms import UserCreationForm, UserLoginForm
from django.views.decorators.csrf import csrf_protect
from django.contrib import messages


@csrf_protect
def register(request, *args, **kwargs):
	form = UserCreationForm(request.POST or None)

	if form.is_valid():
		form.save()
		messages.success(request, 'Cadastro efetuado com sucesso!')
        return redirect('/login')
    
	context = {
        'titulo': 'Registrar',
		'form': form
	}
	
	return render(request, "painel/register.html", context)

@csrf_protect
def login_view(request, *args, **kwargs):
    form = UserLoginForm(request.POST or None)
    if form.is_valid():
        user_obj = form.cleaned_data.get('user_obj')
        login(request, user_obj)
        return redirect("/painel/")
    context = {
        'titulo': 'Registrar',
		'form': form
	}
    return render(request, "painel/login.html", context)


def logout_view(request):
    logout(request)
    return redirect("/login")
